---
date: 2021-05-01
layout: post
tags: [ "css", "legacy", "progressive-enhancement" ]
title: "Styling for Internet Explorer Using Progressive Enhancement"
---

Internet Explorer 11 accounts for less than one percent of total Internet
usage globally and less than half a percent in the United States. Even 
Microsoft's own web apps have stopped supporting it. But on the off chance 
someone *does* use Internet Explorer, what do you want them
to see? This article explains how to employ the concept of *progressive 
enhancement* to provide a moderately *acceptable* experience to users of a 
decaying platform without taking on all of the legacy burden of formally 
targeting it. 

---

### The need for Internet Explorer 11 support

As unfortunate as it is, Internet
Explorer still fulfills its own cursed little niche in the browser market in
enterprise software because it still provides the best support for Active
Directory authentication. That means there are still many office environments
which are still writing new, greenfield web apps which support Internet 
Explorer *exclusively*. If you're applying for jobs in enterprise, your 
future hiring manager may well use Internet
Explorer as their default browser because it's the only way for them to access
their timesheet. 

With that in mind, let's open my own website in Internet Explorer and see what
it looks like from that perspective. 

<div class="center">
    <img alt="an almost entirely unstyled webpage" 
        class="img"
        src="/img/internet-explorer-original.png" 
        title="AHHHHH it burns" />
</div>

Welp. That's problematic. It looks like a website written by someone who hasn't
yet learned *what CSS even is*. Of the handful of styles Internet Explorer is
able to apply, many of them weren't even applied correctly, leaving cutoff 
text. 

### Establishing minimal requirements for Internet Explorer 11

Clearly, if I were going to *target* Internet Explorer, I'd have to almost
completely rewrite all my stylesheets, but I don't necessarily want that. 
Ideally, here's how I'd like things to work out:

* I want to focus my support on the more modern browsers used by 99% of users.
* Wherever possible, I want to use modern, maintainable CSS, even if it's not
supported by Internet Explorer. 
* I want Internet Explorer users themselves to see a small warning encouraging
them to use one of my preferred platforms where I know the site will always 
look a little better, but...
* I want the website to look a bit better in Internet Explorer (not perfect by
any means, but also not offensively bad like it is now). 

### Progressive Enhancement: Theory and Practice

Progressive enhancement is a Third Way for legacy support in which a developer
neither targets a platform nor ignores it. Rather, a developer *supports* a 
platform with some minimal features while providing a *progressively* better 
experience on targeted, modern platforms. 

I recommend bringing a project to a stable version before backfilling code 
to work on Internet Explorer for two reason:

1. Internet Explorer support is secondary, so it makes sense to focus on 
primary platforms.
2. Internet Explorer-compatible styling is inherently less maintainable, so 
it is best to write it last after most of the large refactors have passed. 

Right now my website largely looks how I want it to look on modern browsers. 
I expect there to be
some small changes here and there, but I believe the largest changes to the
style of the site are behind me *for the forseeable future*, so I believe
my website is a good candidate to begin backfilling Internet Explorer support. 

### Internet Explorer's weaknesses and how to use them to our advantage

Primarily, the problems of my website on Internet Explorer come down to two
issues:

1. Internet Explorer does not support CSS variables at all. 
2. Internet Explorer does not support flexboxes *correctly*. 

The second thing is just annoying, but the first is something we can definitely
use to our advantage. It means we can use plain CSS to style things for IE but
override them with CSS variables for modern platforms that *do* support them.

### Backfilling CSS variables with hard values

I'm going to tackle the CSS variables first because it's by far the most
noticeable issue on my site in Internet Explorer. Since Internet Explorer 
does not support CSS variables, I'm going to have to manually go through my
style sheet and fill in the hard values in the line before the line that uses
the variable, so that the hard value will be overwritten by the variable
in browsers that support variables. 

For example, 
*site.css*
```css
h2, h3, h4
{
    color: #0f0;
    color: var(--color-theme-highlight);
}
```

#### But why tho? 

You might be wondering why I'm bothering to keep my CSS variables at all if I 
have to chase each of them with hardcoded values. After all, if I change the
value stored in the variable, I'm now going to have to change each hardcoded
value. 

But recall that targeting IE is not a main goal of my website--I'm just 
providing some minimal secondary support. Yes, ideally, if I ever decide to 
change the value of `--color-theme-highlight`, I should also go in and change
the hardcoded values, but it's not a big deal if one or two of them get
missed for some period of time. 

As counterintuitive as it is, then, I am keeping both the hardcoded values
and the variables because it gives me the maintainability and readability of
variables while providing some minimal support to IE users. 

### Hiding the things that just don't work in Internet Explorer

Most of my other issues come down to my love of flexboxes--and I'm not just 
going to give them up. Instead, for my use case, I'm just going to strip out
the parts of my site that Internet Explorer doesn't display properly. 

We can use CSS variables as a crude feature detection, like so: 

```css
:root
{
    /* Internet Explorer compatibility hacks */
    --display-block: block;
}

.footer
{
    background-color: #070;
    background-color: var(--color-theme);
    box-shadow: 0 0 15px #000; 
    box-shadow: 0 0 15px var(--color-shade-well); 
    display: none;
    display: var(--display-block);
    z-index: 2;
}

```

This hides the website footer with `display: none;` but then overrides it 
with a CSS variable for browsers that support CSS variables.
This is a little hacky, but that's okay because if it breaks, we only 
inconvenience the very few users who use Internet Explorer. 

But of course, at the end of the day, our favorite solution would be for 
our Internet Explorer users to switch. Let's work on encouraging them to do
so. 

### Displaying a message encouraging users to upgrade their browser

There are no surprises here. I'm just reusing the CSS variable hack again so
that my ugly orange warning message displays only for browsers that don't
support CSS variables. 

```css
.div-browser-warning
{
    background-color: #FF8C00;
    color: #000;
    display: block;
    display: var(--display-none);
    padding: 5pt;
}

...

.p-browser-warning
{
    color: #000;
}

.p-browser-warning a
{
    color: #000;
    text-decoration: underline;
}
```

Now I can just add a warning at the top of the page container like: 

```html
    <body>
        <div class="container">
            <div class="div-browser-warning">
                <p class="p-browser-warning">Welcome! We notice you're
                    using an outdated browser, which
                    will result in a degraded experience on this site. Please
                    consider
                    <a href="https://www.mozilla.org/en-US/firefox/new/">a
                        modern, fully supported browser.</a>
                </p>
            </div>
            <header class="flex-row header padded">
```

### In conclusion

At this point, with minimal effort and no structural changes, my site is 
acceptable to look at in Internet Explorer, and my Internet Explorer users now 
know how to reach a better user experience. I'll probably continue to make a 
few incremental improvements, but for now I am quite happy with this
compromise, and I hope that this article has given you a few ideas for how 
to apply the theory of progressive enhancement to make peace with the legacy
users on your platform of choice. 
