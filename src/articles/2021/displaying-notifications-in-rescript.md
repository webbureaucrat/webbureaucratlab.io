---
date: 2021-02-26
title: "Displaying Notifications in ReScript"
tags: [ "documentation", "progressive-web-app", "rescript" ]
---

This article will serve to document and demonstrate the 
[`rescript-notifications`](https://npmjs.org/package/rescript-notifications)
npm package, a complete set of bindings for the JavaScript-compiling ReScript
language 
([formerly BuckleScript/ReasonML](https://rescript-lang.org/blog/bucklescript-is-rebranding)). 
At the close of this article, the reader should be able to
enable and display notifications in an entirely type-safe and functional way.

---

Be advised that there are some limitations to web notifications, and I 
encourage the reader to review the 
[JavaScript Notifications API 
documentation](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API). 
TL;DR: Notifications require an HTTPS 
connection, and they are widely but 
[not universally supported](https://caniuse.com/?search=notifications). 

### How to use this article

This article is *both* a tutorial *and* a demo. While I've included the code
snippets I think are important, I also want to give a tour of the source so 
that the reader can browse it in context.

* *NotificationsDemo.res* is the ReScript source for the scripts on this page.
You can read the whole thing in context by reading the 
[source on GitLab](https://gitlab.com/webbureaucrat/webbureaucrat.gitlab.io/-/blob/master/scripts/demos/notifications-demo/src/NotificationsDemo.res)
* It compiles to 
[*NotificationsDemo.res.mjs*](/scripts/demos/notifications-demo/lib/es6/src/NotificationsDemo.res.mjs), 
which is referenced in a script tag at the bottom of this article. 
* For completeness, the [source of this 
article](https://gitlab.com/webbureaucrat/webbureaucrat.gitlab.io/-/blob/master/src/posts/2021/displaying-notifications-in-rescript.md) 
is also available on GitLab, although there's no magic here and you may not 
need it. 

#### A word from across the web

I plan on adapting this article to be cross-posted on sites like 
[Dev.to](https://dev.to/webbureaucrat) and 
[Functional Works](https://functional.works-hub.com/) by stripping out the 
*demo-ness* of it all. However, if you're getting this from an RSS feed
reader or some such, I'm really not sure how some of this is going to end 
up being displayed for you. 

So feel free to read how you want! But if you'd like to see the notifications,
visit [the live demo site on my blog](https://webbureaucrat.dev/posts/displaying-notifications-in-rescript).

### Setting up the project

You'll need to install the 
[`rescript-notifications`](https://npmjs.org/package/rescript-notifications)
npm package and add it to your *rescript.json* `bs-dependencies` in the usual 
way. 

I'm also using
[`rescript-webapi`](https://www.npmjs.com/package/rescript-webapi), 
although of course you can always just write your own bindings for DOM
manipulation. 

Lastly, I just want to flag that I'm using two `open` statements like so: 
```ocaml
open Notifications;
open Webapi.Dom;
```

(I usually try to limit myself to two or three open statements per file.)

### Reading and requesting Notifications permissions

The first thing to establish is notification permissions. We do this 
through the 
[`Notification.permission`](https://developer.mozilla.org/en-US/docs/Web/API/Notification/permission) 
static property and the 
[`Notification.requestPermission()`](https://developer.mozilla.org/en-US/docs/Web/API/Notification/requestPermission) 
static method. 

For example: 

*NotificationsDemo.res*
```typescript
open RescriptCore;
open Notifications;
open Webapi.Dom;

Webapi.Dom.window -> Window.addEventListener("load", _ => {
  let spanPermissions: option<Dom.element> = 
    Document.querySelector(Webapi.Dom.document, "#span-permission"); 

  let setSpanText = (span: option<Dom.element>, text: string): unit => {
    let _ = span -> Belt.Option.map(span => Element.setInnerText(span, text));
    Console.log("Permission text: " -> String.concat(text));
  };

  let _ = spanPermissions -> setSpanText(Notification.permission);

  let onBtnRequestClick = (_: Dom.event): unit => {
    Console.log("button-request clicked.");
    let _ = Notification.requestPermission()
    -> Promise.then(str => {
        spanPermissions -> setSpanText(str) -> Promise.resolve
      });      
  };

  let _ = Document.querySelector(Webapi.Dom.document, "#button-request")
    -> Belt.Option.map(btn => { 
    btn -> Element.addEventListener("click", onBtnRequestClick);
    Console.log("button-request event added.");
  });

```

As you can see, this demo sets the `span` text to the current notification 
permission state and then wires up an even to the button to ask for 
permission and update the `span` accordingly. You can see the result below: 

<div class="demo">
    <h3>
        Current Notification Permission Status: 
        <span id="span-permission"></span>
    </h3>
    <button id="button-request">Request Permission</button>
</div>

Go ahead and grant permission if you'd like--I am only using notifications
for the purposes of this demo; I promise not to spam you! Ordinarily, I would 
hide the button after permission has been granted because
it only works once, but as a demo this is fine. 

### Constructing notifications to display them

As long as permissions are granted, notifications are displayed as soon as 
they're constructed. 

The `rescript-notifications` binding includes 
[two Notification constructors](https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification), 
`makeWithoutOptions` and `makeWithOptions`. 

Notifications are typed with a type parameter because they can include a 
data object of any datatype. If you don't need it, you can always just use
a throwaway object of some sort. 

The options object includes a lot of properties that the calling code won't
necessarily need (and isn't necessarily widely supported), so the library
includes an `init` function for convenience. 

*NotificationsDemo.res* continues: 
```typescript
  let onBtnNotifyClick = (_: Dom.event): unit => {
    Console.log("button-notify clicked.");
    let _ = Notification.makeWithoutOptions("You have been notified.");
  };

  let onBtnWithOptionsClick = (_: Dom.event): unit => {
    Console.log("button-with-options clicked.");
    let options: NotificationOptions.t<string> = {
      ...NotificationOptions.init(Js.Nullable.return("unused data."))
        , icon: "https://webbureaucrat.dev/img/icons/192.png"
        , body: "with an icon and additional text."
        };
    let _ = Notification.makeWithOptions("You have been thoroughly notified",
                                         options);
  };
  
  let _ = Document.querySelector(Webapi.Dom.document, "#button-notify")
    -> Belt.Option.map(btn => {
    btn -> Element.addEventListener("click", onBtnNotifyClick);
  });

  let _ = Document.querySelector(Webapi.Dom.document, "#button-with-options")
    -> Belt.Option.map(btn => {
    btn -> Element.addEventListener("click", onBtnWithOptionsClick);
  });
});
```

As you can see, the click events call the `make` methods, which, by ReScript
convention, bind to the Notification object constructor. The notifications
will appear as soon as they are constructed. 

The feel free to play with the live demo below: 

<div class="demo">
    <button id="button-notify">Notify without options</button>
    <button id="button-with-options">Notify with options</button>
</div>

### Happy notifying!

This has been a quick demonstration of using JavaScript notifications in a 
type-safe way using ReScript. I hope you have found this helpful and 
informative, and, as always, feel free to 
[reach out](https://gitlab.com/webbureaucrat/webbureaucrat.gitlab.io/-/issues)
with questions or comments. 

<script src="/scripts/demos/notifications-demo/lib/es6_global/src/NotificationsDemo.res.js"
    type="module">
</script>
