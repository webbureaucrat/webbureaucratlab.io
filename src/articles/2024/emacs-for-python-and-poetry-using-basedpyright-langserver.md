---
date: 2024-10-06
tags: [ "basedpyright", "emacs", "poetry-for-python", "python" ]
title: Emacs for Python and Poetry Using `basedpyright-langserver`
---

I am very pleased with my current emacs setup for Python,
but I found setup to be a little tricky. I will document
my setup here for my future self and for any other
Pythonistas looking for a solid emacs config.

---

### Why basedpyright?

Up to this point, I've been using `mypy --strict` on
the command line for all my typechecking needs,
but mypy is quite
slow even for very small codebases, and even in `--strict`
mode it is just not as strict as it could be. It's also
not a language server, which I want for my emacs setup.

[basedpyright](https://docs.basedpyright.com/latest/)
is a fork of pyright with some excellent
improvements in both checking and in reliability. It is
a fast and wonderfully strict typechecker with some
good linting capabilities additionally.

The only drawbacks I am experiencing are that it doesn't
play quite as nicely as mypy with the `boto3-stubs`
clients
for AWS and that it uses nonstandard `# pyright: ignore` 
comments instead of the standard `# type: ignore` 
comments, but I can live with those issues in favor of a 
rigorously strict type-checking experience.

### Configuring your Poetry projects to work with basedpyright or pyright

If you have existing 
projects that use Poetry, you're going to want to fiddle
a little with your virtual environment. 

Go to the root of each of your projects and run the 
following command: 

```bash
poetry config --local virtualenvs.in-project true
```

This will create a *poetry.toml* file if one does not 
already exist and add a corresponding setting. 

This, however, will *not* actually move your existing
virtual environment. In order for this change to take 
effect, you have to remove your existing virtual 
environment, which you can find by running 

```bash
poetry env info --path
```

Don't forget to add the *.venv* path to your *.gitignore*
and the configuration for any other tools you might use,
like pycodestyle, or you'll end up with quite a mess.

Next, you should add a configuration section to your
*pyproject.toml* to tell basedpyright where to look for 
your virtual environment.

```toml
[tool.pyright]
venv=".venv"
venvPath="."
```

You can also do this in *pyrightconfig.json* file if, 
unlike me, you do not already feel completely overrun with
config files for different Python development tools.

### Installing basedpyright as a language server

One very nice thing about basedpyright over pyright is
that it builds the nodejs dependency as a wheel, so you 
can be assured that basedpyright should work on your 
machine regardless of whether you have nodejs installed.

For isolation, it is usually a good idea to 
install executable Python packages using pipx instead of 
pip. Let's go ahead and do that .

```bash
pipx install basedpyright
```

For a sanity check, consider running 

```bash
basedpyright --version
```

Installing basedpyright also gives you access to the 
`basedpyright-langserver` command, but that's not really 
written for users to interact with, so if you run 
`basedpyright-langserver` or 
`basedpyright-langserver --version` or something, you'll
get a nodejs stack trace. 

### Configuring emacs to use `basedpyright-langserver`

I'll assume you as the reader know how to install 
packages from MELPA and have a preferred way to do it. 
Here are all the packages you need: 

* company
* lsp-mode
* lsp-pyright
* lsp-ui
* python-mode

Get those installed and then open your *~/.emacs* or your
*~/.emacs.d/init.el* and add the following: 

```lisp
;; lsp global settings
(add-hook 'after-init-hook 'global-company-mode)
(setq lsp-auto-guess-root t)

;; python
(require 'lsp-mode)
(setq lsp-pyright-langserver-command "basedpyright")
(add-hook 'python-mode-hook (lambda () (require 'lsp-pyright) (lsp)))
```

That should be all you need. 

### A general tip for debugging lsp-mode

If you feel like you've set everything up correctly and 
you're still having trouble getting lsp-mode to find 
packages that should be available in Poetry, one thing
you might try is to go in and delete your 
*~/.emacs.d/.lsp-session-v1*. This will have the effect
of causing lsp-mode to forget about the project root
and force it to look for it again. 

### Happy coding!

I hope this has given you a good head start on your Python
development environment. If you have any questions, well,
I'm a beginner with all this, and I probably can't help 
you, but I will consider merge requests if you have any
extra tips or tricks for using basedpyright with emacs.
