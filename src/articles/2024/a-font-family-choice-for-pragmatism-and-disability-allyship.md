---
date: 2024-09-30
tags: [ "accessibility", "css", "design", "frontend" ]
title: A Font Family Choice for Pragmatism and Disability Allyship
---

There's no need for this to be a full length article, but I've had a couple
occasions recently to evangelize my favorite non-monospace font, and it occurs
to me this might be a good time to collect and share my thoughts about the
font choice I've made for my blog. 

---

### Atkinson Hyperlegible is font choice for disability allyship

All of the text in my blog (except for the code blocks) is 
[Atkinson Hyperlegible](https://www.brailleinstitute.org/freefont/). The linked
page has an excellent and brief rundown of what makes this font special. Rather
than reiterate all those points here, I would encourage you to take just about
sixty seconds to scroll the linked page and return to this article if you'd 
like. 

The long and the short of it is that Atkinson Hyperlegible is a very 
thoughtfully designed font from the standpoint of readability and low-vision
accessibility. 

### Atkinson Hyperlegible is a font choice for any pragmatist

Look, occasionally in technology you come across a good decision with *no*
tradeoffs, only benefits, and nowhere is that more clear than in the world
of disability access on the web. Sometimes sighted people connect from a crumby
internet connection and the images don't load, and in those moments, sighted
people really appreciate your alt text on images, for example. Proper headings
and other best practices for semantic markup help enable navigation for 
screen reader users, but they also improve your search ranking!

This is another example of that. Every user who is at all sighted benefits from
a more legible font choice, myself very much included. 

I am *not* a designer, as anyone looking at my website will tell you. I'm 
technically competent on the frontend, but I am a backend woman, and that's 
where I belong. 

But I'm also a user, and, in that sense, my preferences are as valid as anyone
else's, and as a sighted user I have to say I am **extremely bleeding tired**
of not being able to tell the difference between l, 1, and I. This year 
especially, I'm sure I'm not the only person who is tired of accidentally 
parsing "AI" as "Al" and vice versa. May as well add some A1 sauce because I
am cooked switching back and forth between news stories about generative AI 
and the Al Smith Dinner. 

### Atkinson Hyperlegible may be a font choice for you

I hope to see Atkinson Hyperlegible used more widely, both because I want 
my low-vision neighbors to be able to enjoy the same content I can and because
I benefit from Atkinson Hyperlegible, too. 

So I hope you'll consider using Atkinson Hyperlegible or even consider opening
issues on your favorite projects recommending Atkinson Hyperlegible as an 
option. If not--if it's just not your cup of tea--I hope you've at least 
enjoyed learning a bit of trivia about a unique font that makes interesting
choices.
