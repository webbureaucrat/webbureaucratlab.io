---
date: 2022-03-01
tags: [ "commentary", "feminism" ]
title: "Why I'm Not Writing about Myself for SheCoded"
---

First: I do get the irony of this article; there's no way around the idea that
I am, actually, writing a post for the Dev.to #shecoded tag. But there are so
many things that bother me so much about this annual event as a feminist, so 
instead of writing about my own experiences, I
want to offer some ways to make the annual #shecoded event better. 

---

### Pick a different tag

There are so many things to love about Elizabeth Warren, not the least of which
is that she appropriately cited Coretta Scott King in the debate on the 
famous attorney general confirmation hearings. 

However, I dislike that the soundbyte from McConnell, "Nevertheless, she
persisted" *about Elizabeth Warren* has become a feminist rallying cry. 

If you need a refresher or if you've never closely followed American politics,
Warren was excluded from the attorney general confirmation debate after reading
from a letter from Coretta Scott King on the racism of the attorney general
nominee, Jeff Sessions. Mitch McConnell objected to the letter, and, in 
solidarity, another (male) senator stepped up to read the letter. 

People seized on the idea that Warren was penalized for reading the letter 
while her colleague wasn't, but this is incredibly shallow analysis. Warren
wasn't being punished because she was a woman; she was being punished because
McConnell was in a tight spot politically and made a stupid tactical move in
trying to censor King's letter. Moreover, Warren's colleague wasn't let off the
hook because he was a man but because it was obvious at that point what would 
happen if McConnell kept objecting--every Democratic senator would have lined
up to read the letter and get kicked out, which would further raise the profile
of the letter, which was the opposite of what McConnell was trying to 
accomplish. 

It's worth asking why I find the #neverthelessshepersisted meme so 
objectionable--what's the harm in a shallow meme if it creates a platform
for women to tell their stories. 

1. Gender-based discrimination is a serious issue, and it should be treated
with seriousness. Latching onto a catchy, out-of-context soundbyte diminishes
stories of actual discrimination. 

2. Much more importantly, the misappropriation of the episode is a sad example
of white feminism. The mythology around the silencing of Elizabeth Warren and 
the subsequent outpouring of other women telling their stories effectively 
buried the story of the *actual woman who was being discriminated against and
silenced*: Coretta Scott King. It was *her* words which McConnell objected to 
and *her* story of discrimination being censored. In a way, McConnell 
succeeded in burying the letter *because of* the hashtag, and we cooperate with
King's silencing when we play off of #neverthelessshepersisted, which refers
to Warren. 

### Get rid of #shecodedally

Allyship is great, and incentivising allyship is great, but incentivizing men
to speak out on feminism in a space that's vastly majority-male anyway... isn't
great. If you're a man, I think the most important thing you can do for IWD is
to practice reading-to-understand rather than reading-to-respond. You don't 
have to ultimately agree with every woman's take--I certainly don't--but read
everything with an open mind and just sit with it for a few days before making
up your mind. 

I'm not saying men in general should not say anything, but I think the 
ally hashtag shouldn't be promoted or officially recognized. Men who have 
particular experiences or data they feel they should share certainly should
do so! But donating for every #shecodedally post necessarily puts men in the 
difficult position of chosing between pontificating on a subject they aren't 
fluent in or foregoing a free $20 donation. It's not fair to men, and it is
necessarily going to lead to a "She
Coded" event with a lot more "ally" voices than actual #shecoded posts. 

### Give non-binary people their own day

As a woman, I don't speak for non-binary people, and 
I know not all non-binary people are going to agree with me here, but every
time I see non-binary issues ham-fistedly tacked on to a women's thing, I 
think of [this brutal Reductress article](https://reductress.com/post/4-inclusive-statements-that-arent-women-and-non-binary-people-i-consider-women/). 
Non-binary people are not a kind of woman. Their issues are not usually women's
issues and vice versa. 

Just like #shecodedally posts are going to overwhelm #shecoded posts, #shecoded
posts are going dwarf the number of #theycoded posts. It's not fair to 
non-binary people. 

Alternately, if you absolutely *have* to lump all non-men together, then 
please in the name of all that is holy, at least find a way to actually treat
non-binary people as *equal* in value to women instead of an afterthought. 
If you want to include non-binary people

* Don't call the event "SheCoded" with a mention of #theycoded buried in the
text.
* Don't hold it on a day called, "International Women's Day."
* Don't make the beneficiary an organization called "GirlDevelopIt." 

### In conclusion

In spite of all this, I am glad that Dev.to is actively trying to close the
gender gap in tech. I think the problems with the SheCoded program are 
significant but very solvable, and I hope that, like a well-maintained
software project, it continues to improve over time. 
