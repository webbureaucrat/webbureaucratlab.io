---
date: 2022-07-02
tags: ["commentary", "javascript", "rescript", "tooling"]
title: "De-Microsofting Your Development Environment"
---

Anyone who has been 
following the web ecosystem over the last few
years knows it has been rapidly consolidating
under Microsoft. That consolidation 
economically threatens the open web.
Diversifying your tooling could protect you
as much as diversifying any investment, and 
it's much more convenient than you might 
think.

---

### Why not just use all the Microsoft tooling?

Too often, anything even remotely critical of
Microsoft gets dismissed as archaeic, 
Ballmer-era neckbeard 
bias. I want to be clear here:
I do not hate Microsoft. 
I'm very well aware that Nadella's 
Microsoft is probably among the most 
open-source friendly companies, especially 
for its size. Microsoft's climate goals are 
second to none. Microsoft's F# programming
language is a *pretty good language,* which 
is a big statement coming from me, a person who hates
almost every programming language.

That said, I still have reasons for wanting
to diversify my tooling, and I think those 
reasons are broadly applicable.

* **Policies can change.** Microsoft's shift toward better corporate 
citizenship over the years shows how business practices from change over time. 
The next CEO might be as different from Nadella as Nadella is from his 
predecessors. 
* **Pricing can change.** Vendor lock-in is an insideous thing. Before using
any vendor-specific features of a platform, ask yourself how much of a price
hike your business could reasonably tolerate and, beyond that price,
what the engineering cost of the shift away would be. 
* **Licenses can change.** Remember when Facebook tried to change the terms of
React.js? Small development teams were scrambling to switch, knowing Facebook's
legal team could bury them if they wanted. Facebook backed down because 
React as a product isn't a big part of Facebook's business and because the
market for free front-end frameworks is famously (infamously?) oversaturated. A
company with actual market power over the open web, however, would be a real
threat.
* **Interoperability is expensive.** Microsoft will often face an engineering
cost to adhering to standards it doesn't control in-house. Changing open 
standards requires consortia and drafts and meetings and RFCs--slow, 
democratic processes. And if the open standards require implementing features
it doesn't care about, it will still have to implement them to remain 
interoperable. Therefore, we should assume that the long-term incentives are
against open standards. 
* **Discouraging ICE contracts is still important.** I realize this hasn't 
been in the headlines for a long time, but it's still true that Microsoft has
relationships with DoD and DHS which are troubling to conscientious consumers. 
One reason I avoid Microsoft products is that I want to believe that violating
human rights is *expensive* to their business. 
* **Important technical decisions should never be made by default.** My 
entire career has been spent on the .NET stack, regardless of requirements. 
Don't do that! Figure out what tools will be best for *your* use case, and
don't assume it's what you've always done (and if your developers can't learn 
new things, hire better developers).

None of this is to say that no one should ever use any Microsoft product, but
I would encourage anyone to manage their risk just by considering some 
alternatives, some of the time. I'm going to try to avoid the obvious 
clickbait-y answers ("Use Firefox and LibreOffice and AWS!") and hopefully 
provide some value off the beaten path. 

### Backend-as-a-service

The selling point of Azure--its easy-to-use, seamless integrations--are also
its danger. Consider these alternatives if you don't want to take risks with
price increases. 

* **Parse Platform** is an impressive little open source back end. **Back4App**
offers Parse Platform hosting, but it's portable, so if Back4App ever goes
away or hikes prices, you can simply spin up your own instance and move your 
data. Notably, it has a built-in authentication platform if you want a 
secure way to avoid privacy-hostile third-party OAuth integrations. 
* **Jelastic** is pretty much my favorite cloud platform because it *truly*
commodifies cloud hosting. If you're not familiar, Jelastic is a company that
licenses cloud platform software to anyone with a server farm. You can shop
by price or server region, and if you don't like the service, you can always
choose to do business elsewhere. 
* **PostgREST** doesn't fit neatly into this category, but it's a close 
substitute if you want a quick-and-easy REST API with a little more control 
than you might have in a traditional BaaS. 

### Text editors

Visual Studio Code has taken the 
development world by storm, and not without
good reason. Microsoft's fork of Atom is pluggable
and keyboard-navigable without sacrificing
beginner-friendliness. 

But I'm still a little surprised by its 
popularity. It's my considered opinion 
that for most uses, good ol' reliable emacs
has it beat.

Much digital ink has been spilt against 
the abysmal resource management of 
Electron apps, so I won't dwell on it here, 
but I will say that it's a little absurd 
that in the current year we still have to 
think about the speed and memory consumption
of text editors. Visual Studio and Visual 
Studio Code aren't that slow realtive to 
other full-blown IDEs, but once you free 
yourself from having to actually wait for
your editor to open, it's hard to go back.

But it's not just about *computing* speed--
it's also about human speed. Mousing around 
is slow and, in my opinion, unpleasant 
because it breaks the state of flow. 

As an additional benefit, I really think 
every developer should learn one CLI 
text-editor really well for the times when
getting a GUI set up would be inconvenient or
unrealistic. The last time I tried to use 
the SSH feature on VSCode, it was a buggy
mess. I'd rather just use a battle-tested 
editor like emacs.

### Web Framework

It would be silly to list a bunch of well-known web frameworks for an
audience of developers, but there is one that's worth highlighting: 
Play Framework. Play Framework is based on ASP.NET MVC, and any ASP.NET MVC 
developer would feel right at home, but there are a few reasons why I
prefer Play: 

* C# is, bless its heart, trying to be a better functional language, but it's 
burdened with the legacy of its many years trying to be a Java clone. Scala
is also both object-oriented and functional, but it's more opinionated in its
approach, allowing for cleaner, more uniform code. 

* Play is necessarily stateless, so it scales easily by default. 

* Twirl templates (the Play answer to Razor templates) act as functions that
take parametric inputs and evaluate to HTML. This is much more intuitive 
and flexible than the object-oriented model-binding in ASP.NET. 

### JavaScript that Scales

It's troubling to me that the ongoing (neverending?) debate about static types
on the browser platform has defaulted to "TypeScript or Not TypeScript." 
Conversely, the conversation around "Should you adopt TypeScript?" usually
seems to begin and end with "Do you want to use static types in the browser?"

This is nonsense, honestly. There are lots of mature, production-ready 
languages with robust ecosystems that offer static typing in the browser, and
it's pretty irresponsible to just default to TypeScript without weighing the
benefits against the alternatives. Further, if you *are* so inclined toward
compile-time safety checks, TypeScript frankly offers the least safety in 
favor of being the most JavaScript-like. 

* **Elm** is a combination framework-and-language which guarantees no runtime
errors except for stack overflows (infinite recursion) or through outside 
JavaScript interop. Mutation is not supported by the language at all, which,
like static types, significantly decreases the mental load. If you aren't 
familiar with Haskell-like languages, it may have a significant learning
curve, but the ability to write completely safe UIs in the browser usually
makes it worth it. 

* **PureScript** is another Haskell-like language with a high degree of safety,
but without a baked-in architecture. Despite its safety it has a relatively
short learning curve because it's not overloaded with features. 

* **ReScript** is an OCaml dialect with a very TypeScript-like syntax. If 
you're interested in the safety of functional programming but miss your 
semicolons and braces, this could be a good language for you. 

* And, of course a ton of other languages which didn't start in the browser
have been adapted to it through transpilation or WASM-compilation. ScalaJS and
Fable and Blazor and Rust and js\_of\_ocaml and... you get the idea. And look,
if out of all those you still think TypeScript is your best bet, that's great!
But we're *way* past the point in time where it makes sense to treat it as a 
default.

### Git Hosting

This one just seems obvious to me. If you're considering making just one 
switch, I recommend it be this one because it's just so easy. 

Outside of network effects, I can't see any reason why anyone would 
pick GitHub over GitLab. There are only three significant differences between
GitHub and GitLab as far as I can see: 

1. GitLab is open source.
2. GitLab has a more mature devops platform. 
3. GitHub has an ICE contract. 

end of list. 

When you consider that it only takes two (2) commands to move an entire 
repository from one hosting service to another, it just eludes me why people
use GitHub. 

I have seen maybe one or two job applications that ask for a GitHub username,
specifically, but my answer to that is just to leave my GitHub profile blank
except for a link to my GitLab (and I've only seen this about twice out of
hundreds of job applications).

### Catch-all Consumer Collaboration

Whenever I find myself looking for a privacy-friendly alternative to a 
browser-based  service, I tend to start with 
[disroot.org](https://disroot.org/en/#services). 
In a nutshell, they provide access to free instances of open source online
software, including email, conferencing, and several different services for 
document collaboration. They also have developer-focused services like git
hosting and issue board software. 

I highlight Disroot specifically because it's unusual for a service to be both
very batteries-included consumer-friendly software and also be so open source
and privacy friendly. By default, I don't want to self-host everything and 
administer my own server, but I like knowing that option is open to me if I
want it. 

### Hitting "end task" on lock-in

It's not my intention to moralize at anyone, and I know that there are shops
where a broad Microsoft service contract makes sense, but if you like 
exploring your options and you want to feel like you really *own* your 
projects, then I hope I've provided some good starting points in your search 
for alternatives. 
