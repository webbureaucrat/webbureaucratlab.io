---
date: 2020-12-21
layout: post
tags: ["clean-code", "css", "frontend", "help"]
title: "Designers and front-end folks! How do you name colors?"
---

In my back-end life, naming values that get reused is important to 
maintaining clean code, but somehow I keep turning my CSS variables into 
spaghetti. I've tried three basic strategies, none of which seem to work 
quite right for me. 

---

### Naming by Usage

```css
:root
{
    --color-h1-background: #cecec0;
    --color-h1-text: #405199;
    --color-main-text: #666258;
    ...
}
```

There are a couple of problems with this one. One is: it doesn't really lend 
itself to reusage of colors. If I want to use `--color-main-text` for, say, 
the navigation menu background, I would have to

1. `.nav { background-color: var(--color-main-text); }`  
This is bad because it means `--color-main-text` is misleadingly named. My 
future self would try to change the color of text and end up also changing 
the nav menu. 

2. Define another variable with the same value. I think there's some value in 
this because if all my variables are defined together at the top, I can 
clearly see each place where a certain color is used. The downside is it 
means if I need to make a thematic change that changed every instance of that 
color, I'd have to manually change the value of each variable. 

### Name by color

I'm tempted to name my color variables by naming the actual color. In other 
words, I could name a `--color-brown` and a `--color-teal` for the particular 
brown and blue that go with my theme. 

I like this because it's probably the most readable solution--I can look at 
`--color-brown` anywhere in my stylesheet and clearly call to mind the exact 
shade of brown I'm using. 

The problem is that if I ever needed to make a significant change in my theme, 
let's say from a brown-based theme to a navy one, I'd have to find and 
replace the variable name everyplace it was used. 

### Abstract Naming

My favorite way of naming is the most abstract--that is, naming colors based 
on their role within the theme by designating one color as a primary color 
and then defining *accents* and *highlights* and so forth in relation to that 
theme. 

The problem is that I too quickly run out of these kinds of names (possibly 
relating to the simple fact that I don't know jack about this subject and am 
just making it up as I go). So far I've come up with: 

```css
:root
{
    --color-primary: ...
    --color-highlight: ...
    --color-lowlight: ...
    --color-negative-space: ...
    --color-accent: ...
}
```

I like this because it seems the most open to change. I could entirely switch 
themes just by changing these values, and all the variable names would still 
make sense. 

The problem is it quickly devolves as I continue to add colors, including 
shades of each of these, and I end up being pushed into one of the other 
strategies. 

### Feel free to give me a hand! 

Do you have a naming scheme you really like? Are there more abstract color 
names I should know? Am I just thinking too hard about this? 

How do you plan for changes within your color scheme? 

**[@ me with your help if you can!](https://twitter.com/webburueacrat/)**
