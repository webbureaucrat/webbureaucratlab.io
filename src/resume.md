---
layout: post
title: Resume for Eleanor Holley
eleventyNavigation:
  key: Resume
  order: 400
---

## Eleanor's Professional Experience

### Software Developer, US Railroad Retirement Board
*February 2019-Present*

C# developer focused on modernizing the federal approach to 
data flow. 
* currently serving on the ad hoc **Azure migration team** designing cloud 
services to securely network with on-prem and cloud services in a FedRAMP
High environment. 
* **introduced Docker containers** to solve complex development environment
challenges between secure government Windows laptops to AWS Linux servers.
* **architected WebLisp Business Rules Engine**, a service-oriented rules 
engine based on LISP including compiler, runtime, and MongoDB persistence 
running as independent microservices. 
* **fixed security vulnerabilities and implemented features** 
in the back-end federal benefit web service interfacing between SQL 
Server and DB2 using HP Fortify. 

### Mid-Level Software Developer, State of Indiana
*February 2017-February 2019*

ASP.NET full-stack developer specializing in all load capacity data for local, 
state, and federal bridge structures. 

* **designed databases and data flow** between data warehouses, vendor
solutions, and various web applications using Oracle PL/SQL.
* **wrote BRADIN**, the front-end application portal for centralizing all 
bridge-related information in the state.
* **introduced the first REST microservice** to the Department of 
Transportation.
* **mentored entry-level developers** and served as a technical leader, overseeing developers and contractors on several projects.
* **quickly promoted internally** from entry-level.

### Programming Intern, State of Indiana
*June 2015-September 2015, June 2016-September 2016*

Wrote full-stack applications with hands-on experience from 
database design to front-end development. Wrote a customer satisfaction
survey creation app (Google forms clone) and ConTAX, an application to
manage contacts for local government officials. 

## Eleanor's Education

### Bachelor of Science, Computer Science
**DePaul University, College of Computing and Digital Media**  
*2013-2016*  
GPA 3.0  
Mostly Java, Python, and PL/SQL 

### Microsoft Certified Professional: SQL Server 2016
**December 2016**

## Eleanor's Open Source Contributions

### [Chicago Test Out](https://chicagotestout.gitlab.io/)
a data visualization single-page application (SPA) for COVID-19 data in 
Chicago which works from the same data as the city's data visualization but is
a lot faster and more responsively designed. 

### [ReScript Progressive Web App libraries](https://www.npmjs.com/~webbureaucrat)
ReScript is a functional programming language based on OCaml
which compiles to JavaScript. I 
have contributed a few libraries for progressive web apps including 
[res-elm](https://npmjs.com/package/res-elm),
an NPM package for interoperation between ReScript and Elm languages, 
bindings for interoperation between JavaScript push notification and service 
worker libraries, and bindings to the JavaScript IndexedDB library. 

### [blog](https://webbureaucrat.dev/)
a blog of functional programming tutorials.

## Eleanor's main skills
* database development and design (most SQL dialects, MongoDB)
* object-oriented programming (C#, Java, Python, etc)
* functional programming (ReScript/OCaml, Elm, F#)
* test-driven development (MSTest, JUnit, red/green refactor)
* various cloud platforms (Azure, Jelastic, AWS)
* build automation and DevOps (AZDO, GitLab)
* web platform (HTML5, CSS3, JavaScript)

## Get in touch
https://webbureaucrat.dev/  
https://gitlab.com/eleanorofs/  
https://chicagotestout.gitlab.io/  
https://linkedin.com/in/eleanorholley/  
https://www.npmjs.com/~webbureaucrat  
https://stackoverflow.com/users/5013066/eleanor-holley  
