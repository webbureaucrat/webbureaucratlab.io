---
date: 2020-09-10
layout: post
tags: ["rescript", "bucklescript", "reasonml"]
title: "ReScript: The Module or File Can't Be Found, Unabridged"
---

If you work with BuckleScript, now ReScript, you'll likely come across the 
common build error message, "The module or file $name can't be found" followed
by a few helpful suggestions for how to properly install a ReScript module. 
It's a good error message, but I've found it underestimates me in my ability to
get things wrong, especially if I'm the one who wrote the missing module to 
begin with. I'm going to write a guide while one of these problems is still
fresh in my mind so that I have a checklist to go through the next time I get
frustrated. 

---

I'm going to start with the supply side of the module. If you're referencing
someone else's third party and can assume that their code works on a basic 
level, feel free to skip ahead to "Consuming your ReScript package."

## If you wrote the ReScript module you're having trouble adding

### Check the `name` property in *package.json*

Make sure your package is what you expected it to be in npm. 

### Check the `namespace` property in *bsconfig.json*

The naming convention for BuckleScript is to begin the package name with 
`bs-`, and the emerging naming convention for ReScript is to begin the package
name with `res-`. Meanwhile, the naming convention for modules is to drop the 
prefix. (E. g., `bs-service-worker` and then `open ServiceWorker;`.) However,
when scaffolding the `namespace` attribute of *bsconfig.json* often defaults 
to `true` while scaffolding, which strips special characters like "-" and 
Pascal-cases the name, but otherwise leaves the prefix attached. 

The solution is to set the namespace explicitly. Instead of `true`, use the
module name that fits the module, like `"ServiceWorker"` for 
`bs-service-worker`. 

### Make sure you've run `npm publish` recently. 

Consider setting up CI/CD on `git push`. 


## Consuming your ReScript package

### Make sure your package is properly installed, both places.
Make sure the package is installed by npm into your *node_modules*, and make
sure the package is listed in your bsconfig.json as a dependency. 

### Check the namespace of your package
Your package's namespace is deteremined by the `namespace` attribute of the
*bsconfig.json* (as described above). You need to know it to open it. 

### Make sure you haven't already opened the package at the top of the file
Yes, this is also something I have done. 

This closes out the list of possible causes I'm aware of so far, but I believe
that if I work hard and believe in myself I'm bound to find more ways into 
this compiler error, and when I do, I'll revisit this post. 
