---
date: 2020-08-03
layout: post
tags: documentation
title: Issues and Contribution
---

I've pushed out a few packages recently, and I'd like to take the opportunity
to publish a catch-all document on contribution and issues for each of these. 
This post will walk the user through my current thinking on how to engage
these projects, and I'll keep it up to date as my thinking evolves. 
---

### Abandonment
At this time, each of my projects has my full support. Some of these projects,
though, especially smaller projects, may go for long stretches of time without
receiving new commits simply because they are **finished** for the forseeable
future. If I ever decide to sunset a project later, I'll be sure to list it
here. Absent that, feel free to reach out to me at any time about any of my
projects. 

### Issues
It's my intention to host my projects on GitLab going forward, so if you have
a GitLab account and you're comfortable opening issues, feel free to do so 
directly. You can find a list of my repositories on 
[my GitLab projects page](https://gitlab.com/users/eleanorofs/projects). 

If you don't have an account, no worries. Feel free to contact me another way. 
I've listed some other ways to contact me below. After we've talked over your
issue, I may open an issue on your behalf for tracking and transparency
purposes.

### Contribution

#### Gratuity
There are a couple ways of showing your appreciation for the project. If you're
using and enjoying this project, please consider showing it by giving me a 
star on GitLab. It's very much appreciated. 

Fortunately, I have enough money, but if you'd like to contribute financially,
I strongly encourage you to donate to 
[Rehumanize International](https://www.rehumanizeintl.org/donate) and let me
know that you did. I'll consider it an incentive to keep working on these
projects, so it'll be like you're donating to both. 

#### Code
If you have something you'd like to see added to one of my projects, it's 
probably faster to do it yourself than to wait around for me to do it. I'm 
open to merge requests on each of my projects, which you can find on 
[my GitLab projects page](https://gitlab.com/users/eleanorofs/projects). 

If you'd like, feel free to start a conversation with me before you spend time
on your contribution. I can give you
an up-or-down to make sure you aren't wasting your time and give you my
half-baked thoughts on style for easier merging. The various ways to
get in touch are listed below.

I do want to say ahead of time: For my own projects, I tend to like my code
*just so*. I'm still formulating a style guide in my head. A few bullet points:

 * 80-character lines are very important to me. 
 * Type annotations are required wherever feasible.
 * Code comments are **always** encouraged. 
 * I generally prefer lambda expressions with an explicitly named parameter. 
 (E. g., `map(data => myFunction(data))` over `map(myFunction)`. This isn't a 
 hard-and-fast rule, though, and it's not necessary especially if you're 
 fighting with the 80-char line rule. 

Please don't take it personally if I nit-pick your contribution. I am fully
self-aware that most of my judgements are arbitrary, and I definitely don't
think any less of you as a developer if I decide to send something back to 
you for a silly reason. I promise I am a good person when I'm not reading
other people's code.

### Coordinated disclosure of security vulnerabilities
I've listed several ways of contacting me below. Please only discuss 
vulnerabilities over an end-to-end encrypted channel. 
 * My KeyBase chat is published below. 
 * I also have a Signal account and a ProtonMail account. If you would like 
 to use one of those, feel free to ask me for it by opening an issue or 
 contacting me on Twitter or some other way. 
 * I'm open to using whatever end-to-end encrypted service you prefer, even if
 I don't already have an account, just let me know. 
 
After the issue has been resolved, I will post it as an issue and then close
the issue out for transparency purposes, so that users know that there has been
an issue and that it's important to update.

Thank you so much in advance for working on these issues!

### Questions
If you have any questions about any of my projects or you'd like to see 
something documented that isn't already, please feel free to contact me any 
of the ways listed below. 

I encourage you, though, to start on StackOverflow and then shoot me a link. 
I like StackOverflow and I'd like to see the young communities I'm involved in
like Elm and ReasonML to thrive there by building a public, searchable body 
of knowledge. 

### Contacting Me
For public discussions: 
 * @ me or DM me on [Twitter](https://twitter.com/webbureaucrat).
 * Open an issue on the appropriate 
 [GitLab project page](https://gitlab.com/users/eleanorofs/projects). 
 
For private discussions (for example, security vulnerabilities):
 * Add me on KeyBase chat at eleanorholley.
 * DM me on [Twitter](https://twitter.com/webbureaucrat) for my Signal
 account or my ProtonMail account. 

Feel free to reach out--I'm happy to shoot the breeze with any randos for 
all things tech-related. 

