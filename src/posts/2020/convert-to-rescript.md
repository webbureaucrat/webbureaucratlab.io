---
date: 2020-11-04
layout: post
tags: [ "bucklescript", "emacs", "rescript", "reasonml"]
title: "How to Upgrade from BuckleScript to ReScript"
---

I have a weakness: I have never seen an update I didn't want to adopt
immediately. Betas? No thanks. Give me the nightly build. New major version of 
the Linux kernel? I'll just run the installation without stopping to grab my
charger. So when BuckleScript announced a whole new language syntax (along
with a rebrand to "ReScript"), I got 
excited and immediately wanted to transpile everything. This post
will document the simple process step-by-step.

---

### The need for the upgrade

To be honest, there isn't much of one. As the ReScript package maintainers have
emphasized, the new compiler will not break code in the old syntax, as long
as that old syntax is still in *.re* files. 

I have as good a reason as any, though. I have a BuckleScript package
called [bs-elm-es6](https://npmjs.com/package/bs-elm-es6), and I 
want new users who are unfamiliar with the old syntax to be able to read it, so
I transpiled it to the new ReScript syntax and pushed it to a new package 
called [res-elm](https://npmjs.com/package/res-elm). I also have a 
[demo website](https://elmandbucklescript.gitlab.io/)
for `bs-elm-es6` that functions as a project template and is also an important
part of its documentation. I want to continue this template/documentations for
users of `res-elm` (like myself), so that's what I'm going to start with here. 

### Upgrading to the ReScript compiler

I'd like to leave the BuckleScript version for historical reasons just in case
there are users who aren't ready to make the switch, so I forked the repository
rather than push drastic changes to the old one. To make the page work in 
GitLab pages, I created a new group called 
[elmandrescript](https://gitlab.com/elmandrescript) and created a new 
repository to match the domain, 
[elmandrescript.gitlab.io](https://elmandrescript.gitlab.io/) Then I cloned
that repository to my local workbench. 

The next step, for me, is to navigate to my local BuckleScript repository and 
do a quick sanity check. 

```bash
npm run build
```

This should succeed, but it never hurts to make sure it does before you make
changes.

Next, it's time to upgrade the compiler. For me, the easiest way to 
upgrade an npm
package is just to uninstall and reinstall (without making any other changes in
between. 

```bash
npm r bs-platform
npm i bs-platform
```

This should upgrade you to the latest version. Note: as long as both commands
succeeded, this is not a breaking change. You should make sure that 
`bs-platform` is properly installed by running your build command once again.

```bash
npm run build
```

This should succeed, which is a good reminder: we don't have to migrate 
everything all at once. If we want, we can use the old compiler for *.re*
files that use the old syntax.

### Upgrading old BuckleScript code to ReScript Syntax

The `bsc -format` command will translate our new code automatically. Run it on
each of your scripts. 

```bash
npx bsc -format src/Index.re > src/Index.res
```

If you build after this command using `npm run build` again, you will get a 
compiler error because you now have two modules that have the same name. It is 
safe to remove the old *.re* file. 

```bash
rm src/Index.re
npm run build
```

Now your project should build successfully. 

### Review the new *.res* file and revise

It's worth reviewing your new code and looking at the changes. In particular,
the transpiler sometimes deletes whitespace that I prefer to have. (I'm a 
stickler for 80-character lines, which is a challenge in ReScript). It also 
deletes semicolons at the ends of lines because semicolons are optional in 
ReScript. 

I like semicolons, and the emacs `reason-mode` still needs them to determine
tab width, so I'm going to put at least some of them back. 

I'm also going to splurge and update some other code dependencies. In
particular, I want to take this opportunity to upgrade from 
[`bs-elm-es6`](https://npmjs.com/package/bs-elm-es6) to 
[`res-elm`](https://npmjs.com/package/res-elm/) for a consistently readable
syntax throughout the codebase. (And mostly because `res-elm` is mine and I 
really need to test it!)


### Enjoy the new ReScript syntax

If the above steps succeeded, you have now moved successfully from BuckleScript
7 to ReScript 8. I'll update this post with the new npm package location as 
soon as its available. 
