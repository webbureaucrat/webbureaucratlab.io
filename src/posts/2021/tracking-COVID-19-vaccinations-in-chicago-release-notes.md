---json
{
    "date": "2021-01-01",
    "tags": [ "documentation", "data-visualization" ],
    "title": "Tracking COVID-19 Vaccinations in Chicago: Release Notes"
}

---

Happy New Year, and thank you for reading! I am delighted to announce that 
the City of Chicago has started reporting daily statistics on COVID-19 
vaccinations, and so am I. This article will detail recent changes as well 
as changes to come in the near future. 

---

### New tab added to display vaccination data

Right now, the new tab for displaying vaccination data is already in 
production at https://chicagotestout.gitlab.io/ with some basic information. 
Right now I'm charting the total vaccinations given out per day but displaying
more detailed information on the table below. 

At the time of this post, it looks like Chicago is administering about 3,000
vaccinations per day, all of them first vaccinations (meaning no one in the
city so far has received a complete two-dose vaccination series). 

I've written previously on 
[the importance of citing official sources](/posts/chicago-test-out-moving-to-by-test-data). It is my intention that all of the data on the vaccinations tab
will come from this 
[city vaccination data set](https://data.cityofchicago.org/Health-Human-Services/COVID-19-Daily-Vaccinations-Chicago-Residents/2vhs-cf6b). 

### Future plans include more detailed charting and useful data columns

Eventually, someone somewhere in the city will get their second vaccination, 
and I'll be able to meaningfully differentiate between first vaccinations and
total vaccinations in my vaccination chart. I intend to have a three line chart
containing the volume of first vaccines, final vaccines, and the total per 
day. 

I don't plan on reporting the raw volume of Chicagoans *cumulatively* 
vaccinated because I don't find that number particularly interesting, but I 
will add a new chart for *percent of Chicagoans cumulatively vaccinated* 
because I think that's a more useful metric. 

Perhaps more importantly, the city is graciously providing columns indicating
the percent of each demographic group that has been vaccinated, including 
how many Chicagoans have been vaccinated by age group. I am not displaying 
this data right now but intend to add those columns in the coming days and 
weeks.

### Happy Reading!

I started writing this site early last year to channel my fears and feel 
informed and empowered in these dark times. For the first time in a long time,
my site is reporting some unmitigated good news, so I hope I can share. Stay 
safe and God bless! 
