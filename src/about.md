---
layout: layouts/post
title: About Eleanor Holley
eleventyNavigation:
  key: About Me
  order: 300
---

### Eleanor's passion

Though her degree focused on object-oriented development, Eleanor fell in love
with functional programming starting with LISP and Scala. She's fascinated by
the connections between functional and object-oriented best practices and is 
determined to continue finding ways to bring the thread-safety and referential
transparency of functional programming into object-oriented, enterprise-focused
languages like C#, Java, and JavaScript. 

Eleanor is a proud public servant and believes that good code and good 
infrastructure are critical pieces of good government and that the usability
and accessibility of government services are moral issues. Accordingly, she
is proud of her role as a technical leader on continuous integration best 
practices, cloud adoption, and solid software architecture. 

### Eleanor's biography

Eleanor Holley started programming in high school using **Visual BASIC, C++, 
and Java**
and fell in love with the craft, earning the Computer Science Award
twice. 

She went on to study Computer Science at DePaul University from 2013 to
2016 in a curriculum that was mostly **Java and Python** based and 
included several classes in relational database design and development
in **Oracle PL/SQL**. 

Along the way, Eleanor worked as a web development intern at the State of 
Indiana where she worked at the Department of Local Government Finance. 
she learned
the web fundamentals of **HTML, CSS, and JavaScript** as well as 
**ASP.NET and C#,** (which was a very easy transition because C# is 
based on Java). She also learned a different SQL dialect: **TSQL**, 
and designed and implemented database schemas and implemented stored
procedures to underly the two apps she wrote for the department for 
which she worked two summers. With this real-world experience
to supplement her university database programming studies, she was able
to pass the **SQL Server Microsoft Certified Professional** exam. 

After graduating from college a year early, Eleanor returned to 
work as a web developer 
for the State of Indiana at the Department of Transportation
for two years, using **ASP.NET and C#** and **PL/SQL**. Her skill 
was very quickly recognized and in an unusual move she was 
promoted from entry level developer to mid-level less than 
a year after starting, taking on 
more responsibilities as a **technical lead** in mid-sized
projects, solving complex **data engineering** problems and
learning **Angular, TypeScript, and cross-platform mobile 
development**.

Today, Eleanor is grateful to continue her work in public service
as a developer in the federal government at the U. S. Railroad
Retirement Board. While she continues to specialize in **C#**, she
has picked up a number of frameworks including **Drupal and ASP Classic**.
It is Eleanor's belief that a senior-level developer should
be able to learn and be productive in a new language after about
one week of study. 
