---
layout: layouts/post
title: Portfolio for Eleanor Holley
eleventyNavigation:
  key: Portfolio
  order: 150
---

These are a few of the open source projects I'm particularly 
proud of. For a fuller picture
of my work, see [my GitLab](https://gitlab.org/eleanorofs) and 
[my npm](https://www.npmjs.com/~webbureaucrat). 

### [Chicago Test Out](https://chicagotestout.gitlab.io/)
<a href="/tags/rescript/" class="post-tag">rescript</a>
<a href="/tags/data-visualization/" class="post-tag">data-visualization</a>
<a href="/tags/elm/" class="post-tag">elm</a>

As 
[listed on the Civic Tech Field Guide](https://civictech.guide/listing/chicago-test-out/), 
I created [Chicago Test Out](https://chicagotestout.gitlab.io/) out of 
frustration with the local newspapers who were only using state-level data
for their COVID-19 tracking. I used to 
check this site every day for the latest data. 

<hr class="hr-spacer" />

### [res-elm](https://npmjs.org/package/res-elm)
<a href="/tags/bucklescript/" class="post-tag">bucklescript</a>
<a href="/tags/elm/" class="post-tag">elm</a>
<a href="/tags/rescript/" class="post-tag">rescript</a>

This is the only ReScript/Elm interop package which requires no module
loader. It lives at the intersection of two very niche languages, but if 
type-safety is your passion, check it out. 
[This package](https://www.npmjs.com/package/res-elm/) is used in production
to facilitate Chicago Test Out. 

<hr class="hr-spacer" />

### Progressive Web App ReScript bindings
<a href="/tags/progressive-web-application/" class="post-tag">progressive-web-application</a>
<a href="/tags/rescript/" class="post-tag">rescript</a>

I wrote these bindings because I decided that JavaScript Service Workers 
would be a good way to incrementally add ReScript to projects like 
Chicago Test Out and this Eleventy blog.

[`rescript-notifications`](https://www.npmjs.com/package/rescript-notifications)
is documented and demonstrated in 
[this article](/posts/displaying-notifications-in-rescript/). Both 
`rescript-notifications` and 
[`rescript-push`](https://www.npmjs.com/package/rescript-push) are
dependencies of 
[`rescript-service-worker`](https://www.npmjs.com/package/rescript-service-worker), 
which is also [documented in an 
article](https://webbureaucrat.dev/articles/writing-service-workers-in-rescript/)
on this site.
If you would like to see a demo, it is already in use on both
[Chicago Test Out](https://chicagotestout.gitlab.io/) and this
website itself.

<hr class="hr-spacer" />

### [BrotherJacoba.org](https://brotherjacoba.org)

This is definitely the easiest project I've ever done, but I'm proud to be the
maintainer for 
[my Secular Franciscan fraternity](https://brotherjacoba.org). 
I'm also proud to have written
a site for the modern era that requires absolutely no JavaScript. 

