let
  # nixos-24.11
  # 2025-02-12
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/archive/44534bc021b85c8d78e465021e21f33b856e2540.tar.gz";
  pkgs = import nixpkgs { config = {}; overlays = []; };
in

pkgs.mkShellNoCC {
  packages = with pkgs; [
    cacert 
    git
    less
    lesspipe
    nodejs_23
  ];
}
