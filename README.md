# [webbureaucrat](https://webbureaucrat.dev/)

## Eleanor Holley's tech blog

## Potential future topics
- [ ] development for nonprofits
- [ ] how I started actually shipping side projects
- [ ] using rescript-vigil
- [ ] using rescript-reap
- [ ] using rescript-fetch
- [ ] passing env vars to postgres
- [ ] fixing JWT in PostgREST
- [ ] setting a role password to a pg config param
- [ ] nginx
- [ ] fixing `cargo build` in nixos
