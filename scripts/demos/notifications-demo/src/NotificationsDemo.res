open RescriptCore;
open Notifications;
open Webapi.Dom;

Webapi.Dom.window -> Window.addEventListener("load", _ => {
  let spanPermissions: option<Dom.element> = 
    Document.querySelector(Webapi.Dom.document, "#span-permission"); 

  let setSpanText = (span: option<Dom.element>, text: string): unit => {
    let _ = span -> Belt.Option.map(span => Element.setInnerText(span, text));
    Console.log("Permission text: " -> String.concat(text));
  };

  let _ = spanPermissions -> setSpanText(Notification.permission);

  let onBtnRequestClick = (_: Dom.event): unit => {
    Console.log("button-request clicked.");
    let _ = Notification.requestPermission()
    -> Promise.then(str => {
        spanPermissions -> setSpanText(str) -> Promise.resolve
      });      
  };

  let _ = Document.querySelector(Webapi.Dom.document, "#button-request")
    -> Belt.Option.map(btn => { 
    btn -> Element.addEventListener("click", onBtnRequestClick);
    Console.log("button-request event added.");
  });

  let onBtnNotifyClick = (_: Dom.event): unit => {
    Console.log("button-notify clicked.");
    let _ = Notification.makeWithoutOptions("You have been notified.");
  };

  let onBtnWithOptionsClick = (_: Dom.event): unit => {
    Console.log("button-with-options clicked.");
    let options: NotificationOptions.t<string> = {
      ...NotificationOptions.init(Js.Nullable.return("unused data."))
        , icon: "https://webbureaucrat.dev/img/icons/192.png"
        , body: "with an icon and additional text."
        };
    let _ = Notification.makeWithOptions("You have been thoroughly notified",
                                         options);
  };
  
  let _ = Document.querySelector(Webapi.Dom.document, "#button-notify")
    -> Belt.Option.map(btn => {
    btn -> Element.addEventListener("click", onBtnNotifyClick);
  });

  let _ = Document.querySelector(Webapi.Dom.document, "#button-with-options")
    -> Belt.Option.map(btn => {
    btn -> Element.addEventListener("click", onBtnWithOptionsClick);
  });
});
