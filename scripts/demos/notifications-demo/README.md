# Notifications Demo

This is the source code for the demo for the `rescript-notifications` binding
project. A full discussion of the demo and walkthrough of the code can be 
found in the [article for which this demo was 
written](https://webbureaucrat.dev/articles/displaying-notifications-in-rescript/).
