open ServiceWorker;
open ServiceWorkerGlobalScope;

let path = "/SW.js";

let _ = self -> navigator
  -> ServiceWorkerNavigator.serviceWorker
  -> ServiceWorkerContainer.Register.withoutOptions(path)
  ;
