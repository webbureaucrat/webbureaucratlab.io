open RescriptCore;
open ServiceWorker;
open ServiceWorkerGlobalScope;

/* local bindings */
@new external makeExn: string => exn = "Error";

/* configuration */
let version = "0.0.12";
  
let assetCacheName = String.concat("webbureaucrat-static-", version);

let runtimeCacheName = String.concat("webbureaucrat-runtime-", version);

let assets = [
  Cache.str("/css/default.css"),
  Cache.str("/css/fonts/atkinson-hyperlegible/atkinson-hyperlegible.css"),
  Cache.str("/css/fonts/atkinson-hyperlegible/WOFF2/Atkinson-Hyperlegible-Bold-102a.woff2"),
  Cache.str("/css/fonts/atkinson-hyperlegible/WOFF2/Atkinson-Hyperlegible-BoldItalic-102a.woff2"),
  Cache.str("/css/fonts/atkinson-hyperlegible/WOFF2/Atkinson-Hyperlegible-Italic-102a.woff2"),
  Cache.str("/css/fonts/atkinson-hyperlegible/WOFF2/Atkinson-Hyperlegible-Regular-102a.woff2"),
  Cache.str("/css/fonts/DejaVuSansMono-webfont.woff"),
  Cache.str("/css/menu.css"),
  Cache.str("/css/pygments.css"),
  Cache.str("/css/prism-base16-monokai.dark.css"),
  Cache.str("/css/site.css"),
  Cache.str("/css/util.css"),
  Cache.str("/img/icons/192.png"),
  Cache.str("/manifest.json"),
  Cache.str("/portfolio/"),
  Cache.str("/resume/"),
  Cache.str("/about/")
];

/* helper methods */

let fromCache = (req: Fetch.Request.t, cacheName: string):
  promise<Fetch.Response.t> =>
    caches(self) -> CacheStorage.open_(cacheName)
    -> Promise.then(cache => cache -> Cache.Match.withoutOptions(req))
    -> Promise.then(matching => switch(matching) {
        | Some(m) => Promise.resolve(m);
        | None => Promise.reject(makeExn("no-match"));
    })
;

let fromNetwork = (req:Fetch.Request.t):
    promise<Fetch.Response.t> =>
    Fetch.send(req);

let precache = (): Promise.t<unit> =>
    caches(self)
    -> CacheStorage.open_(assetCacheName)
    -> Promise.then(cache => cache -> Cache.addAll(assets));

let addToCache = (req: Fetch.Request.t, cacheName: string):
  promise<unit> => {
    caches(self)
      -> CacheStorage.open_(cacheName)
      -> Promise.then(cache => cache -> Cache.add(#Request(req)))
  };

let assetStrategy = (req: Fetch.Request.t, cacheName: string):
    promise<Fetch.Response.t> => {
      let result = req -> Fetch.Request.fromRequest({})
          -> fromCache(cacheName);

      /* update cache iff. item already exists in cache */
      let _ = result -> Promise.then(_ => addToCache(req, cacheName));

      /* don't wait on the update to return */
      result
    };

let runtimeStrategy = (req: Fetch.Request.t, cacheName: string):
  promise<Fetch.Response.t> => {
    let result = req -> Fetch.Request.fromRequest({}) -> fromNetwork
        -> Promise.catch(_ => req -> Fetch.Request.fromRequest({})
                         -> fromCache(cacheName)
                        );

    let _ = addToCache(req, cacheName);
    result
  };

/* event listeners */
self -> set_oninstall(event => {
  Console.log("The service worker is being installed.");
  event -> Notifications.ExtendableEvent.waitUntil(precache());
});

self -> set_onfetch(event => {
  let req = event -> FetchEvent.request;
  let resp: Promise.t<Fetch.Response.t> =
  req -> Fetch.Request.fromRequest({})
    -> assetStrategy(assetCacheName)
    -> Promise.catch(_ => req
                     -> Fetch.Request.fromRequest({})
                     -> runtimeStrategy(runtimeCacheName));
  event -> FetchEvent.respondWith(#Promise(resp));
});
