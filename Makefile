all: public

node_modules/: package.json package-lock.json
	npm ci --verbose

package-lock.json:
	npm ci --verbose

public: _11ty css img manifest.json node_modules/ \
	scripts/demos/notifications-demo/lib/ \
	scripts/sw-loader/lib/ src/ SW.js
	npx eleventy
	rm -rf public/scripts/
	cp -r scripts/ public/scripts/

scripts/demos/notifications-demo/node_modules/: \
	scripts/demos/notifications-demo/package.json \
	scripts/demos/notifications-demo/package-lock.json
	cd scripts/demos/notifications-demo/; \
	npm ci

scripts/demos/notifications-demo/lib/: \
	scripts/demos/notifications-demo/node_modules/ \
	scripts/demos/notifications-demo/src/
	cd scripts/demos/notifications-demo/; \
	npm run build

scripts/sw-loader/lib/: scripts/sw-loader/node_modules/ scripts/sw-loader/src/
	cd scripts/sw-loader/; \
	npm run build

scripts/sw-loader/node_modules/: scripts/sw-loader/package.json \
	scripts/sw-loader/package-lock.json
	cd scripts/sw-loader/; \
	npm ci

scripts/wb-service-worker/node_modules/: \
	scripts/wb-service-worker/package.json scripts/package-lock.json
	cd scripts/wb-service-worker/; \
	npm ci

SW.js: scripts/wb-service-worker/node_modules/ scripts/wb-service-worker/src/
	cd scripts/wb-service-worker/; \
	make SW.js
	cp scripts/wb-service-worker/SW.js .
