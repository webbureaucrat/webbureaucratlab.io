---
date: 2023-05-13
tags: ["auth", "database", "postgresql"]
title: "Creating a User Database in Postgres"
---

This article will go over the basics of creating a database of users including 
usernames, user IDs, hashed passwords, and hashed authentication tokens using
the `pgcrypto` Postgres extension. Along the way, we'll explore basic postgres
features like DDL, function sytax, and built-in functions. 

### A basic Postgres environment in Docker

The easiest way to get started with Postgres is in Docker.

Start with a *.env* file that contains your prefered environment variables. 
The `POSTGRES_PASSWORD` environment variable is required, but there are others
available in the standard image. (Consult the 
[official documentation](https://hub.docker.com/_/postgres/).)

*database.env*
```
POSTGRES_USER=postgres_user
POSTGRES_PASSWORD=postgres_password
POSTGRES_DB=huntergatherer
```

Next, create a docker-compose.yml to run the postgres image. 
I'm developing against the `latest` tag for now. 

*docker-compose.yml*
```yaml
version: "3.8"
services:
  database:
    env_file: "database.env"
    image: postgres:latest
    ports:
      - "5432:5432"
```

Finally, create a volume to load the initialization scripts. We'll write such
a script in the next section. 

*docker-compose.yml*
```yaml
version: "3.8"
services:
  database:
    env_file: "database.env"
    image: postgres:latest
    ports:
      - "5432:5432"
    volumes:
     - ./database/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
```

### Writing a table for users in Postgres

Let's start in on the initialization scripts. Create a directory 
*docker-entrypoint-initdb.d* inside your *database* folder. We will put our
initialization scripts there. The Postgres image will read these folders in 
alphabetical order, so it's a good idea to (a) organize our scripts numerically
while (b) leaving room between numbers to make room for more scripts. 

I'm going to number my script "010" because I believe I want it to be the first
script I will run, but I am allowing myself a hedge. I will of course give it
a descriptive name beyond that. 

Inside this file, start with a user table: 

*./database/docker-entrypoint-initdb.d/010-schema-auth.sql*
```sql
create schema if not exists auth;

---------------------------------users-----------------------------------------

create table if not exists
auth.users (
  password text not null check (length(password) < 512),
  user_id uuid primary key default gen_random_uuid(),
  username text unique not null
);
```

In my opinion, it's a good idea to have an internal way to refer to users (i. 
e. `user_id`) so that your design is more open to allowing username changes. 
Going over the rest of the syntax line by line is outside the scope of this
article. 

#### Hashing passwords in PostgreSQL with `pgcrypto`

Hashing passwords is supported in Postgres through the `pgcrypto` extension. 
To use it, we need only to enable it on our schema. This will load the relevant
functions and stored procedures as though we had created them within our 
schema. 

*./database/docker-entrypoint-initdb.d/010-schema-auth.sql*
```sql
create schema if not exists auth;

create extension if not exists pgcrypto with schema auth;

---------------------------------users-----------------------------------------

create table if not exists
auth.users (
  password text not null check (length(password) < 512),
  user_id uuid primary key default gen_random_uuid(),
  username text unique not null
);
```

In my opinion, the best way to hash a password inside Postgres is with a 
database trigger. While I'm not broadly fond of triggers because they tend to
obfuscate behavior, it is *so vitally* important that passwords are always 
saved hashed that to me it makes sense to put the behavior inside a trigger. 
This eliminates the possibility that a developer create or update users while
forgetting to hash the password. 

With that, let's grind our teeth and create a database trigger. 

*./database/docker-entrypoint-initdb.d/010-schema-auth.sql*
```sql
create schema if not exists auth;

create extension if not exists pgcrypto with schema auth;

---------------------------------users-----------------------------------------

create table if not exists
auth.users (
  password text not null check (length(password) < 512),
  user_id uuid primary key default gen_random_uuid(),
  username text unique not null
);

create or replace function
auth.hash_password() returns trigger as $$
begin
  if tg_op = 'INSERT' or new.password <> old.password then
    new.password = auth.crypt(new.password, auth.gen_salt('bf'));
  end if;
  return new;
end
$$ language plpgsql;

drop trigger if exists hash_password on auth.users;
create trigger hash_password
  before insert or update on auth.users
  for each row
  execute procedure auth.hash_password();
```

### Creating authentication tokens

We can't expect our client to send the username and password with every
request. For one thing, caching the password in a client is insecure. For 
another, running `bcrypt` on every request will dramatically slow our 
application. What we need is a table of login tokens that represent a session.
These are more secure than a password because they can be ephemeral and 
(unfortunately unlike many user's passwords) cryptologically securely randomly
generated. 

Create a token table like so: 

```sql
-----------------------------------auth.tokens---------------------------------

create table if not exists
auth.tokens(
  user_id uuid not null,
  token text primary key,
  created timestamp default current_timestamp,
  valid boolean not null default true,
  constraint fk_tokens_to_users foreign key(user_id)
             references auth.users(user_id)
);
```

Tracking the `created` timestamp will give us the option to expire tokens by 
age (though that is left as an exercise for the reader). The `valid` column
will allow us to expire tokens immediately (if, for example, the user 
chooses to log out of a public computer). 

These tokens are basically ephemeral passwords, and they *should absolutely* 
be hashed, but they don't have to be hashed as slowly as our `bcrypt`ed 
passwords. `SHA256` is sufficient. Let's create a trigger to that effect. 

```sql
create or replace function
auth.hash_token() returns trigger as $$
begin
  if tg_op = 'INSERT'
  then new.token = encode(auth.digest(new.token, 'sha256'), 'hex');
  end if;
  return new;
end
$$ language plpgsql;


drop trigger if exists hash_token on auth.tokens;
create trigger hash_password
  before insert or update on auth.tokens
  for each row
  execute procedure auth.hash_token();

```

### Writing basic authentication functions in Postgres

Next, we're going to want logic to register, log in, and log out of the 
application. To do that, let's start with a few functions to support
these functions. This will help us keep our code nicely factored, readable,
and DRY. 

```sql
```
