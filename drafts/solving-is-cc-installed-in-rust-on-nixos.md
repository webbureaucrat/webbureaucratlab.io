---
date: 2023-07-21
tags: [ "nixos", "rust" ] 
title: 'Solving "error occurred: Failed to find tool. Is `cc` installed?"'
---

Just a quick bite today: This article is on a `cargo build` error which I've
run into specifically on NixOS. The error is as follows: 

```
error occurred: Failed to find tool. Is `cc` installed?
```

The solution, it turns out, is just to make sure `gcc` is also installed. 

```bash
nix-shell --packages gcc
```

Then `cargo build` works!

This should probably be considered a bug but in the meantime I hope this helps
someone else. 
