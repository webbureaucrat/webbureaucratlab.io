---
date: 2023-09-26
tags: ["desktop", "rusqlite", "rust", "sql", "sqlite"]
title: "Local Storage for Desktop Apps Using Rust and Rusqlite"
---

Continuing my new adventure in desktop development, I'd like a clean way of 
interacting with hard drive storage, and SQLite seems like a good way. There
are lots of bindings for SQLite in Rust, but a particularly widely used one is
rusqlite, so let's begin.

---

### Installing Rusqlite in a Rust project

Let's start with a clean slate. 

```bash
cargo init rusqlite-kata
cd rusqlite-kata
```

The first thing to study 
about rusqlite is the feature flags; there are a lot of possibilities. For a 
full list of feature flags and their uses, visit the 
[README documentation](https://github.com/rusqlite/rusqlite)

I'm going to start with three feature flags: 
* `bundled` - to bundle an up-to-date SQLite version with the application. 
* `chrono` - to serialize and deserialize `Datetime`s. 
* `uuid` - to serialize and deserialize `uuid`s. 

`uuid` also comes with its own set of flags. I'm going to use the `v4` feature
flag to use v4 UUIDs. 

*Cargo.toml*
```bash
[package]
name = "rusqlite-kata"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
chrono = "0.4.31"
rusqlite = { version = "0.29.0", features = ["bundled", "chrono", "uuid"] }
uuid = { version = "1.4.1", features = ["v4"] }
```

### Checking if a table exists using `rusqlite` for Rust and Sqlite

Let's start by checking if a table exists so we know whether or not to create
it. 

```rust
use rusqlite::{Connection, Error, Rows};

fn set_up_database(connection: &Connection) -> Result<(), Error>{
    let table_exists_statement_text =
        "SELECT count(*) FROM sqlite_master
         WHERE type='table' AND name='transactions';";
    let mut table_exists_statement = connection.prepare(
        table_exists_statement_text
    )?;
    let mut rows: Rows<'_> = table_exists_statement.query([])?;
    let mut table_exists: i32 = 0;
    match rows.next()? {
        None => {
            table_exists = 0;
        },
        Some(row) => {
            table_exists = row.get(0)?; 
        }
    };
    Ok(())
}

fn main() {

}
```

Wow, that's a mess of code just to check if a table exists! Let's refactor. 

Consider this `execute_scalar` function. It takes a connection, a query, and
some parameters of the query and returns the first column of the first row. 

```rust
fn execute_scalar<T: FromSql, P: Params>(
    connection: &Connection, statement_text: &str, params: P
) -> Result<Option<T>, Error> {
    let mut statement: Statement = connection.prepare(statement_text)?;
    let mut rows: Rows<'_> = statement.query(params)?;
    match rows.next()? {
        None => Ok(None),
        Some(row) => row.get(0)
    }            
}
```

Now we can call `execute_scalar` from a function that checks if a given table
exists. 

```rust
fn table_exists(
     connection: &Connection, table_name: &str
) -> Result<bool, Error> {
    let statement_text =
        "SELECT count(*) FROM sqlite_master
         WHERE type='table' AND name=?1;";
    let exists_option =
        execute_scalar(connection, statement_text, [table_name])?;
    match exists_option {
        None => Ok(false),
        Some(exists) => Ok(exists)
    }
}
```

### Creating the table

Next let's write a function we can use to create a table if the table isn't 
there. Since we aren't querying data, we can use `execute` instead of query. 

```rust
fn create_table(connection: &Connection) -> Result<(), Error> {
    let statement_text =
        "create table stocks.transactions (
             date text not null,
             price integer not null,
             ticker text not null,
             uuid text primary key
        );";
    connection.execute(statement_text, ())?;
    Ok(())
}
```

This is effectively a parameterized statement with no parameters, so we pass 
in the empty tuple for parameters.

### Modeling the table

Let's go ahead and model the table. This will help us insert and retrieve data.

```rust
use chrono::naive::NaiveDateTime;
use rusqlite::{Connection, Error, Params, Rows, Statement};
use rusqlite::types::FromSql;
use uuid::Uuid;

struct Transaction {
    date: NaiveDateTime,
    price: i32,
    ticker: String,
    uuid: Uuid
}
```

Now we can use a parameterized statement to insert Transactions into the
database.

```rust
fn insert(
    connection: &Connection, row: Transaction
) -> Result<(), Error> {
    let statement_text =
        "insert into stocks.transactions (date, price, ticker, uuid)
             values(?1, ?2, ?3, ?4);";
    connection.execute(
        statement_text, (row.date, row.price, row.ticker, row.uuid)
    )?;
    Ok(())
}
```

### Retrieving data

Now let's work on retrieving data. Let's start with a select query like 
`select date, price, ticker, uuid from transactions;` Then let's `prepare` it 
like the select statement from before: 

```rust
    let statement_text =
        "select date, price, ticker, uuid from transactions;";
    let mut statement: Statement = connection.prepare(statement_text)?;
```

Now let's query that data set and convert it to our DTO struct: 

```rust
    let rows = statement.query_map([], |row| {
        Ok(Transaction {
            date: row.get(0)?,
            price: row.get(1)?,
            ticker: row.get(2)?,
            uuid: row.get(3)?
        })
    })?;

```

This gives us a `MappedRows` of `Transaction`s. Let's go ahead and collect 
the results into a `Vec` of `Transaction`s. 

```rust
    let transaction_results: Vec<Result<Transaction, Error>> =
        rows.into_iter().collect();
```

Then we can collect that vector into a single result: 

```rust
    transaction_results.into_iter().collect()
```

Putting it all together, we get: 

```rust
fn select(connection: &Connection) -> Result<Vec<Transaction>, Error> {
    let statement_text =
        "select date, price, ticker, uuid from transactions;";
    let mut statement: Statement = connection.prepare(statement_text)?;
    let rows = statement.query_map([], |row| {
        Ok(Transaction {
            date: row.get(0)?,
            price: row.get(1)?,
            ticker: row.get(2)?,
            uuid: row.get(3)?
        })
    })?;
    let transaction_results: Vec<Result<Transaction, Error>> =
        rows.into_iter().collect();
    transaction_results.into_iter().collect()
}
```
