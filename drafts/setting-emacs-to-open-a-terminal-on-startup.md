---
date: 2025-01-17
tags: ["emacs", "productivity"]
title: Setting Emacs to Open a Terminal on Startup
---

Recently, I've started using my shell more in emacs 
rather than switching between emacs and a separate 
terminal window. I really like doing things this way
because I only need to keep one set of keybindings in my 
head at a time, and it's nice to be able to kill (copy)
and yank (paste) suggested commands right from the 
keyboard. This article documents my simple setup.

### The Windows situation

As far as I can tell, there is no way to use `M-x term`
and `M-x ansi-term` on Windows (please let me know if I am 
wrong!) However, there are still pretty good options for 
Windows users. 

### Shell

`M-x shell`, sometimes called, "the inferior shell", 
is the oldest way to open a shell in emacs, and
it works okay in Windows if it's configured just right,
but it has some drawbacks, too. Emacs has no default 
shell on Windows, so if you run `M-x shell` it will prompt
you for an executable by default. I use MSYS2 bash, which 
is distributed with git as Git Bash.

To configure emacs's `M-x shell` to use the version of bash 
that comes with git for Windows, set the shell-file-name
to the executable for bash. For example, on my system, 
git lives in *Program Files*, so I have included this 
line in my *~/.emacs.d/init.el*:

```lisp
(setq shell-file-name "C:/Program Files/Git/bin/bash.exe")
```

Make sure you do not use *git-bash.exe* instead. That 
opens the default Git Bash terminal in its own window. 

If you close and reopen emacs and use the `M-x shell`
command, you should get a bash environment. However, it's 
not the best experience. The first thing you'll probably 
notice is it can't handle colors very cleanly. You'll
probably see terminal colors, and you'll also see color
escapes cluttering the buffer. Interactive programs won't
work as well as you'd expect them to work, either. Most 
frustratingly, setting the shell as the initial 
default buffer seems to significantly degrade the 
experience even further.

I'm glad to have bash configured for shell for when I want
it, but it's not what I'm using by default. Instead, I'm
using eshell.

### eshell

eshell requires no configuration on emacs, even on
Windows, because it a native feature of emacs. It's
batteries-included. However, parts of it may feel a little
stripped compared to the full bash experience. For
example, I tend to alias `ls` to `ls --color=auto -AF`, but
in eshell, there's no `-F` flag on `ls`. Most things will 
generally work how you expect them to, though, and the
color experience just works better. 

My ideal setup is to run eshell as the default buffer
instead of scratch or the welcome screen when I'm running 
emacs using the emacs command or the runemacs shortcut and
to instead display the specified file or files when I'm 
running the emacs command with arguments. 

I start with the following line in my 
*~/.emacs.d/init.el*:

```lisp
(setq initial-buffer-choice 'eshell)
```
Now when I close emacs and reopen it, it drops me right
into a shell on startup, and this is cool, but if I run
emacs as a terminal command like 
`emacs ~/.emacs.d/init.el`, I get 
two buffers open at once. I don't care for this behavior,
and I can change it with the following two lines.

```lisp
(when (cdr command-line-args)
  (setq initial-buffer-choice nil))
```

Now when there's a command-line argument, eshell won't
open as well.

### term and ansi-term

Fortunately, on Linux, we have better options available
to us through term and ansi-term. As far as I can tell, 
all the meaningful differences 
between M-x term and M-x ansi-term are in the past, so
for the purposes of this article I'm just going to talk
about term. 
