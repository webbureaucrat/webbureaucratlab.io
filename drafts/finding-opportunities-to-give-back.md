---
date: 2022-09-01
tags: ["commentary", "growth", "volunteerism"]
title: "Finding Opportunities to Give Back"
---

At some point in your life you probably have thought or will think about ways
of giving back to your community. We software developers tend to be well-paid, 
and we've all seen what bad technology choices do to small organizations doing
their best to make ends meet. I've worked as a web dev for a number of 
organizations and wanted to publish my thoughts on my experience and give the
advice I wish I had heard when I was first starting out. 

---

### A couple of disclaimers

* This guide will probably skew heavily toward web development because that's
where most of my experience is and because that's what most nonprofits need.

* Don't assume that the experiences I share here reflect on the organizations
I've worked with because I haven't consistently publicized every organization
I've talked to about their technology. Naturally, the negative experiences
(or just bad fits) were the shortest-lived, lasting just a few weeks sometimes.

* I currently work full-time for a very large nonprofit organization, which is
a wholly different situation from what I describe here. I do not write about
my professional work, and my employer likes it that way and very much wants 
you to know that the opinions expressed here do not necessarily represent 
their opinions.

### Who should volunteer as a software developer? 

No, I'm sorry, but at least in my opinion, the answer is not, "everyone!"

Please understand, I do not say this to gatekeep development overall, but 
there are a few important things to consider. 

Primarily, consider your level of experience. If you're a beginner, 
volunteering for a nonprofit to build your skills sounds like a win-win, but
in practice I would strongly discourage a beginner developer from volunteering
unsupervised, just like I would strongly discourage an experienced developer
from volunteering for a tech project outside their core competencies. 

Take web development, for example. Putting together a website for a local
nonprofit sounds like an easy win since it's mostly static content--there's
very little logic involved. 

But give it a little more thought. Do you know the ins and outs of
accessibility standards? Can you be sure that a blind person could easily
navigate something you write? How good are you at controlling hosting costs? 
Are you comfortable saying your website is 
secure, knowing anyone with an internet connection could be your opponent? 

You may think that surely offering your skills is still better than not, just
like you apply for jobs you aren't sure you qualify for and let the employer
decide, but there are two important distinctions there: 

1. The job you think is a reach probably has a whole development staff to help
supervise and mentor you, so you never really get in over your head. If you're
offering your skills to a nonprofit, you may be the only technologist 
offering. You'd be flying without a net. 
2. The hiring manager you'd be applying to is (hopefully!) qualified to judge
your skillset. This rarely true at the kinds of small nonprofits that need 
help. If you volunteer for a nonprofit, you may very well be crowding out 
other, better qualified volunteers, and if your product is really bad the 
people you're volunteering under may not have any way of knowing. 

If you are a beginner and you are dead set on lending a hand, I don't want to
shoot you down entirely, but you should look for projects where you'll be 
working for someone who can work over you and check your work, but if you're 
working unsupervised, a good rule of thumb is that if you think this 
volunteer position would be a goood opportunity to learn new things or sharpen
your skills, don't take the position. You should only 
be doing things you could do in your sleep. 

### How should you find opportunities? 

Frankly... it seems like this question should be, like, a lot easier. Every 
organization has technology needs, and those needs tend to be pretty expensive,
but matching those needs to qualified techologists seems to not be a solved
problem on a societal level. (On a related note, this is probably a good idea
for an admittedly not-very-lucrative startup.)

#### [Taproot Plus](https://www.taprootplus.org/)

[Taproot Plus](https://www.taprootplus.org/) is a listing service for 
nonprofits looking for professionals. Using the website is straightforward, 
but like all platforms, there are some tricks for searching and matchmaking.

* The time commitment numbers don't necessarily mean a lot. Devotees of
Agile Method will tell you that sweeping, long-term estimates mean very 
little. Imagine how little they mean when they come from someone who isn't
a technologist, and the person who listed the opportunity probably isn't. 
* Many people don't know the differences between a web designer and a web
developer, and as a volunteer you'll likely be handling both roles anyway. If
you're searching for one, then, you may as well also search for the other. 
* If it seems like there's not a lot of detail in the listing, trust that
instict. Leaders of nonprofits are generally good people, but they're also
humans, and humans have the tendency to undervalue anything they get for free, 
and the barriers to *asking* for a volunteer are pretty low. It's too easy 
to just have some half-baked idea and ask a techologist to do it all without 
committing to spec-ing out the organizations needs or thinking about what 
users really want. Those projects *will* fail. 

#### Cold emails

If you're looking for a nonprofit to volunteer for, you may feel the urge
to proactively reach out to local or national organizations and just tell them
what your skills are and what level of commitment you would be comfortable 
with. 

There's little harm in it, but you should consider resisting that urge. 
Anyone with a contact page is automatically going to get spammed all the time
with scammers claiming they found vulnerabilities or selling entity
verification certificates or some other tech nonsense. They've probably also
had a slew of well-meaning people who claim to be able to build great 
big things and then
leave after six months when they've bit off more than they can chew. In my 
experience, cold emails aren't a good way to introduce yourself to an 
organization. They don't always leave a good impression, and they don't 
result in emails back. 

#### The Hard Way

Of course the hard way, the old-fashioned way, is to volunteer for a bunch of
organizations as a regular pair of hands and wait for them to *have* some 
technology needs you know you can fill. 

Follow all the local organizations on social media, subscribe to their 
newsletters, and of course show up to help, and eventually someone, somewhere
will put the word out that they're looking for a technologist, or even just 
that they wish there were an easier way to do the things that you know 
exactly how to do. 

Personally, for all the time I've spent on Taproot Plus, this is the only way
that has worked for me, and I'm so happy with all of the organizations I've
worked with. 

#### Stay tuned

This has been the first of what I think will be a series on software 
development volunteerism. In a coming artile, I plan to lay out how to 
onboard onto a nonproft and help make technical decisions for with them. 
