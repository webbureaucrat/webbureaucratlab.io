---
date: 2024-09-04
tags: ["chumsky", "documentation", "json5", "languages", "rust"]
title: "Fault-Tolerant Parsing in Rust Using Chumsky"
---

Recently, I've gotten into building parsers in Rust. I've had a lot of fun with
the [`pest`](https://pest.rs) library. Its parsing expression grammar is a 
treat to work with, and I would definitely recommend it for building parsers 
for machine-to-machine data interchange formats. 

However, I'm finding pest isn't the right tool for the job for parsing 
languages for humans. Humans make mistakes, and unlike machines, it matters to
humans *how many* mistakes there are in their code, and they'd like to know
about as many of their mistakes at a time as can be discerned from the text. 
This requires not merely a straight-up-or-down, pass/fail, 
one-error-message-at-a-time parser but rather a fault-tolerant parser, i. e., a
parser which, when it encounters an error, can continue parsing and finding 
more errors and can return a partial abstract syntax tree in addition to a 
list of errors. 

Therefore, for human-readable and human-maintained formats, I am switching to 
[Chumsky](https://docs.rs/chumsky/latest/chumsky/index.html). Chumsky's 
builder-pattern library is not nearly as easy to work with as pest, but that's
because it is capable of handling a lot more complexity, 

Let's play around with Chumsky to create a robust, fault-tolerant parser for
JSON5. 

### Starting a new project in Rust

I generally think there's a strong case to be made 
for descriptive and intuitive project names, so you
might expect me to call this project "json5" or "json5-parse" or 
something, but here's the thing: I 
hate the name JSON5. I think the name JSON5 is so 
misleading that it's dishonest, so I'm going to 
ignore the name and pick an obfuscating name in 
protest. 

```bash
cargo new --lib joseph-parse
cd joseph-parse
```

and the sanity check:

```bash
cargo build
cargo test --no-run
cargo test
```

All these should succeed.

### Installing Chumsky

Chumsky is straighforward to install. The default installation comes with 
sensible default feature flags. You can just run 

```bash
cargo add chumsky
```

### Starting an Abstract Syntax Tree

The value-add of reading this tutorial over, say, reading the 
[official Chumsky JSON example](https://github.com/zesterer/chumsky/blob/main/examples/json.rs)
is that I intend to go a little slower and start a little simpler and talk 
about types as I go. 

So with that, let's create an Expression enumeration that defines only a small
subset of JSON5.

*src/expression.rs*
```rust
pub enum Expression {
    Null,
    Bool(bool)
}
```

This enumeration can represent three values, all of which are literals, which
is a nice simple place to start. 

### Writing our first parser with Chumsky

Just as JSON5 expressions are composed of more JSON5 expressions, parsers are
composed of more parsers. Let's write the simplest parser we can possibly
think of: a parser that just parses if a string is equal to null. 

```rust
use chumsky::error::Simple;
use chumsky::Parser; // necessary for "to"
use chumsky::primitive::just;
use crate::expression::Expression;

fn null_parser<'a>() -> impl Parser<&'a str, Expression, Error = Simple<&'a str>> {
    just("null").to(Expression::Null)
}
```

For my code, personally, I'm not crazy about the idea of a "prelude". You might
notice that the Chumsky example code uses `use chumsky::prelude::*;` and I
don't. I won't judge you for using the prelude, but to me, it's worth a few
extra keystrokes to be able to clearly see where all of the values I'm
using are coming from. 

If you inspect the types here, you'll see that what we've actually got is a 
[`To`](https://docs.rs/chumsky/latest/chumsky/combinator/struct.To.html)
of a 
[`Just`](https://docs.rs/chumsky/latest/chumsky/primitive/struct.Just.html), 
and while it wouldn't hurt to have the type signature of our `null_parser`
function be more specific, I think that using the broader `impl Parser` 
return type helps clarify the intent of our code and leaves it open to
different implementations that may not include `to` and `just`. 

[`Simple`](https://docs.rs/chumsky/latest/chumsky/error/struct.Simple.html) 
(as readers can helpfully see from my prelude-free `use` statement) is an Error
type. I chose it because at present it is the most robust error type in the
crate. I say "at present" because it appears the next version of Chumsky will
include an additional error type called `Rich`. If, at the time you read this, 
Chumsky has shipped crate version 1.0, please 
[open an issue](https://gitlab.com/webbureaucrat/webbureaucrat.gitlab.io/-/issues)
on this article.

This code will not compile as-is; you'll get an error because `To`s and 
`Parsers` require that our AST type implements `Clone`. Let's fix that. 

*src/expression.rs*
```rust
#[derive(Clone)]
pub enum Expression {
    Null,
    Bool(bool)
}
```

Now if you add our two modules to the top of your *lib.rs* file, the project
should compile. 

*src/lib.rs*
```rust
mod expression;
mod parser;

// continues...
```
